package toolbox

import (
	"fmt"
	"os"
)

// Oops takes a string and writes it to stderr. It then exist the program.
// Intended as a convenience function around an issue
func Oops(s string) {
	fmt.Fprintln(os.Stderr, s)
	os.Exit(1)
}

func Oopse(e error) {
	fmt.Fprintln(os.Stderr, e.Error())
	os.Exit(1)
}

func Oopsf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, format+"\n", args...)
	os.Exit(1)
}
